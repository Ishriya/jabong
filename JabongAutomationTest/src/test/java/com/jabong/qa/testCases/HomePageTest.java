package com.jabong.qa.testCases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.jabong.qa.base.TestBase;
import com.jabong.qa.pages.HomePage;
import com.jabong.qa.pages.KidsPage;
import com.jabong.qa.pages.LoginPage;
import com.jabong.qa.util.TestUtil;

public class HomePageTest extends TestBase{
	LoginPage LoginPageobj;
	HomePage HomePageObj;
	TestUtil TestUtilObj;
	KidsPage KidsPageObj;
	
	public HomePageTest(){
		super();
	}
	
	//Test cases should be independent with each other.
	//Before each test case - launch browser and login
	//@Test - execute test case
	//after each test case - close the browser
	
	@BeforeMethod
	public void setUp(){
		initialization();
		TestUtilObj = new TestUtil();
		LoginPageobj = new LoginPage();
		KidsPageObj = new KidsPage();
		HomePageObj = LoginPageobj.login(prop.getProperty("username"), prop.getProperty("password"));
	}
	
	@Test(priority=1)
	public void verifyHomePageTitleTest(){
		String homePageTitle = HomePageObj.verifyHomePageTitle();
		System.out.println(homePageTitle);
		Assert.assertEquals(homePageTitle, prop.getProperty("HomePageTitle"), "Home Page title not matched");
	}
	
	@Test(priority=2)
	public void verifyUserLabelTest(){
		Assert.assertTrue(HomePageObj.verifyUSerLabel());
	}
	
	@Test(priority=3)
	public void clickOnKidsLinkTest(){
		KidsPageObj = HomePageObj.clickOnKidsLink();
	}
	
	@AfterMethod
	public void tearDown(){
		driver.quit();
	}
	

}
