package com.jabong.qa.testCases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.jabong.qa.base.TestBase;
import com.jabong.qa.pages.HomePage;
import com.jabong.qa.pages.LoginPage;

public class LoginPageTest extends TestBase{
	
	LoginPage LoginPageobj;
	HomePage HomePageObj;
	
	public LoginPageTest(){
		super();
	}
	
	@BeforeMethod
	public void setUp(){
		initialization();
		LoginPageobj = new LoginPage();
	}
	
	@Test(priority=1)
	public void LoginPageTitleTest(){
		String PageTitle = LoginPageobj.verifyLoginPageTitle();
		Assert.assertEquals(PageTitle, "Online Shopping Site: Buy Women, Men, Kids Fashion & Lifestyle in India - Jabong");
	}
	
	@Test(priority=2)
	public void jabongLogoTest(){
		boolean flag = LoginPageobj.validateLogo();
		Assert.assertTrue(flag);
	}
	
	@Test(priority=3)
	public void LoginTest(){
		HomePageObj = LoginPageobj.login(prop.getProperty("username"), prop.getProperty("password"));
	}
	
	@AfterMethod
	public void tearDown(){
		driver.quit();
	}

}
