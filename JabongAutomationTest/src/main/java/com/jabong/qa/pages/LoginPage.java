package com.jabong.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.jabong.qa.base.TestBase;

public class LoginPage extends TestBase {
	
	//Page factory or object repository 
	@FindBy(id="signin-model-wishlist")
	WebElement signinLink;

	@FindBy(id="login-email")
	WebElement signinEmail;
	
	@FindBy(id="login-pwd")
	WebElement signinPWD;
	
	
	
	
	
	@FindBy(id="btn-login")
	WebElement signinButton;
	
	@FindBy(xpath="//button[class='close']")
	WebElement closeSign;
	
	@FindBy(xpath="//a[contains(text(),'Signup')]")
	WebElement signUp;
	
	@FindBy(xpath="//a[contains(text(),'help / Support')]")
	WebElement helpSupport;
	
	@FindBy(xpath="//a[contains(text(),'Jabong')]")
	WebElement JabongLogo;
	
	//Initializing the page factory
	public LoginPage(){
		PageFactory.initElements(driver, this);
	}
	
	//Actions
	public String verifyLoginPageTitle(){
		System.out.println(driver.getTitle());
		return driver.getTitle();
	}
	
	public boolean validateLogo(){
		return JabongLogo.isDisplayed();
	}
	
	public HomePage login(String un, String pwd){
		signinLink.click();
		signinEmail.sendKeys(un);
		signinPWD.sendKeys(pwd);
		signinButton.click();
		return new HomePage();
	}
}
