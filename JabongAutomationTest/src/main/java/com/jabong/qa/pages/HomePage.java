package com.jabong.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.jabong.qa.base.TestBase;

public class HomePage extends TestBase {
	
	//Page factory or object repository 
		@FindBy(xpath="//a[contains(text(),'Hey, jyoti')]")
		WebElement userNameLabel;

		@FindBy(xpath="//a[@href='/kids/']")
		WebElement kidsLink;
		
	//Initializing the page factory
		public HomePage(){
			PageFactory.initElements(driver, this);
		}

	//Actions
		public String verifyHomePageTitle(){
			//System.out.println(driver.getTitle());
			return driver.getTitle();
		}
		
		public boolean verifyUSerLabel(){
			System.out.println(userNameLabel.getText());
			return userNameLabel.isDisplayed();
		}
		
		public KidsPage clickOnKidsLink(){
			kidsLink.click();
			System.out.println(driver.getTitle());
			return new KidsPage();
		}
}
