package com.jabong.qa.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestBase {
	
	public static WebDriver driver;
	protected static Properties prop;

	public TestBase(){
		try {
		prop = new Properties();
//		FileInputStream fin = new FileInputStream("D:/Automation/Workspace/JetAirwaysTest/src/"
//				+ "main/java/com/jetAirways/qa/config/config.properties");
		
		FileInputStream fin = new FileInputStream(System.getProperty("user.dir")+ 
				"\\src\\main\\java\\com\\jabong\\qa\\config\\config.properties");
			prop.load(fin);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		 
	}
	
	public static void initialization(){
		String BrowserName = prop.getProperty("browser");
		if(BrowserName.equals("chrome")){
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-notifications");
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ 
    				"\\src\\main\\java\\com\\jabong\\qa\\config\\chromedriver.exe"); 
            driver = new ChromeDriver(options); 
		}else if(BrowserName.equals("firefox")){
            System.setProperty("webdriver.gecko.driver", System.getProperty("D:/All drivers and utilities/chromedriver.exe")); 
            driver = new FirefoxDriver();
		}
		
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.get(prop.getProperty("url"));
	}
	
}
